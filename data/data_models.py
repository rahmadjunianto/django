# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class UserMusren(models.Model):
    user_id = models.IntegerField(db_column='USER_ID', blank=True, null=True)  # Field name made lowercase.
    rw_id = models.IntegerField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'USER_MUSREN'


class ActionLog(models.Model):
    id_log = models.BigAutoField(primary_key=True)
    id_user = models.IntegerField()
    aksi = models.CharField(max_length=255, blank=True, null=True)
    entity_id = models.IntegerField(blank=True, null=True)
    keterangan = models.TextField()
    waktu = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'action_log'


class Migrations(models.Model):
    migration = models.CharField(max_length=255)
    batch = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'migrations'


class NotificationLog(models.Model):
    id_user_subject = models.TextField(db_column='ID_USER_SUBJECT', blank=True, null=True)  # Field name made lowercase.
    id_user_target = models.TextField(db_column='ID_USER_TARGET', blank=True, null=True)  # Field name made lowercase.
    predikat_aksi = models.TextField(db_column='PREDIKAT_AKSI', blank=True, null=True)  # Field name made lowercase.
    object_aksi = models.TextField(db_column='OBJECT_AKSI', blank=True, null=True)  # Field name made lowercase.
    affected_routes = models.CharField(db_column='AFFECTED_ROUTES', max_length=255, blank=True, null=True)  # Field name made lowercase.
    affected_model = models.CharField(db_column='AFFECTED_MODEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    object_sebelum = models.TextField(db_column='OBJECT_SEBELUM', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    ip_address = models.CharField(db_column='IP_ADDRESS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    # id = models.BigAutoField()
    object_sesudah = models.TextField(db_column='OBJECT_SESUDAH', blank=True, null=True)  # Field name made lowercase.
    is_read = models.NullBooleanField(db_column='IS_READ')  # Field name made lowercase.
    usulan_id = models.CharField(db_column='USULAN_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'notification_log'


class PasswordResets(models.Model):
    email = models.CharField(max_length=255)
    token = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'password_resets'


class Sessions(models.Model):
    id = models.CharField(unique=True, max_length=255)
    user_id = models.IntegerField(blank=True, null=True)
    ip_address = models.CharField(max_length=45, blank=True, null=True)
    user_agent = models.TextField(blank=True, null=True)
    payload = models.TextField()
    last_activity = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'sessions'


class Users(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(unique=True, max_length=255)
    password = models.CharField(max_length=255)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    login = models.IntegerField(blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    app = models.IntegerField(blank=True, null=True)
    level = models.IntegerField(blank=True, null=True)
    rw_id = models.IntegerField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    kel_id = models.IntegerField(db_column='KEL_ID', blank=True, null=True)  # Field name made lowercase.
    kec_id = models.IntegerField(db_column='KEC_ID', blank=True, null=True)  # Field name made lowercase.
    validasi = models.IntegerField(blank=True, null=True)
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    role = models.SmallIntegerField(blank=True, null=True)
    bidang_id = models.IntegerField(db_column='BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_id = models.IntegerField(db_column='SUB_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    bidang_skpd_id = models.IntegerField(db_column='BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_skpd_id = models.IntegerField(db_column='SUB_BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tapd_id = models.IntegerField(db_column='TAPD_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'users'


class UsersCopy1(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(unique=True, max_length=255)
    password = models.CharField(max_length=255)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    login = models.IntegerField(blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    app = models.IntegerField(blank=True, null=True)
    level = models.IntegerField(blank=True, null=True)
    rw_id = models.IntegerField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    kel_id = models.IntegerField(db_column='KEL_ID', blank=True, null=True)  # Field name made lowercase.
    kec_id = models.IntegerField(db_column='KEC_ID', blank=True, null=True)  # Field name made lowercase.
    validasi = models.IntegerField(blank=True, null=True)
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    role = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users_copy1'
