from django.db import models

# Create your models here.
class Users(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(unique=True, max_length=255)
    password = models.CharField(max_length=255)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    login = models.IntegerField(blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    app = models.IntegerField(blank=True, null=True)
    level = models.IntegerField(blank=True, null=True)
    rw_id = models.IntegerField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    kel_id = models.IntegerField(db_column='KEL_ID', blank=True, null=True)  # Field name made lowercase.
    kec_id = models.IntegerField(db_column='KEC_ID', blank=True, null=True)  # Field name made lowercase.
    validasi = models.IntegerField(blank=True, null=True)
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    role = models.SmallIntegerField(blank=True, null=True)
    bidang_id = models.IntegerField(db_column='BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_id = models.IntegerField(db_column='SUB_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    bidang_skpd_id = models.IntegerField(db_column='BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_skpd_id = models.IntegerField(db_column='SUB_BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tapd_id = models.IntegerField(db_column='TAPD_ID', blank=True, null=True)  # Field name made lowercase.
    object = models.Manager()

    def __str__(self):
        return "{}".format(self.id)
    class Meta:
        managed = False
        db_table = u'"DATA\".\"users"'