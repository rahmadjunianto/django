from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^rekapAjuan/$', views.rekapAjuan, name="rekapAjuan"),
    url(r'^excel_view/$', views.excel_view, name="excel_view"),
    url(r'^rekapAjuanopd/$', views.rekapAjuanopd, name="rekapAjuanopd"),
    url(r'^rekapAjuanopd/(?P<opd_id>[0-9]+)/$', views.rekapAjuanopd, name="rekapAjuanopd"),
    url(r'^excel_view_rekapAjuanopd/(?P<opd_id>[0-9]+)/$', views.excel_view_rekapAjuanopd, name="excel_view_rekapAjuanopd"),
    url(r'^rekapAjuanKegiatanopd/$', views.rekapAjuanKegiatanopd, name="rekapAjuanKegiatanopd"),
    url(r'^rekapAjuanKegiatanopd/(?P<opd_id>[0-9]+)/$', views.rekapAjuanKegiatanopd, name="rekapAjuanKegiatanopd"),
    url(r'^excel_view_rekapAjuanKegiatanopd/(?P<opd_id>[0-9]+)/$', views.excel_view_rekapAjuanKegiatanopd, name="excel_view_rekapAjuanKegiatanopd"),
    url(r'^kegiatanEntry/$', views.kegiatanEntry, name="kegiatanEntry"),
    url(r'^kegiatanEntry/(?P<opd_id>[0-9]+)/$', views.kegiatanEntry, name="kegiatanEntry"),
    url(r'^excel_view_kegiatanEntry/(?P<opd_id>[0-9]+)/$', views.excel_view_kegiatanEntry, name="excel_view_kegiatanEntry"),

    url(r'^renja2/$', views.renja2, name="renja2"),
    url(r'^renja2/(?P<opd_id>[0-9]+)/$', views.renja2, name="renja2"),
    url(r'^excel_view_renja2/(?P<opd_id>[0-9]+)/$', views.excel_view_renja2, name="excel_view_renja2"),
    #
    url(r'^rekapAjuan/$', views.rekapAjuan, name="rekapAjuan"),
]