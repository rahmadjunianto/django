from django.shortcuts import render
from django.db import connection
from django.http import HttpResponse

from django.contrib.auth.models import User
# Create your views here.
import xlwt

def rekapAjuanopd(request,opd_id=None):
    if opd_id is None:
        wh=''
    else:
        wh="where skpd_id="+opd_id

    cursor = connection.cursor()
    cursor.execute('select * from "MUSRENBANG".new_laporan_ajuan_program_kegiatan_opd '+ wh)
    Data = cursor.fetchall()
    cursor = connection.cursor()
    cursor.execute('select "SKPD_ID","SKPD_NAMA" from "REFERENSI"."REF_SKPD"')
    Opd = cursor.fetchall()
    context = {
        'title'          : 'Ajuan Program OPD',
        'active_rekapAjuanopd'    : 'active',
        'Data'           : Data,
        'Opd'           : Opd,
        'Opd_id'           : opd_id,
    }
    return render(request,'monitoring/rekapAjuanopd.html',context)
def excel_view_rekapAjuanopd(request,opd_id=None):
    if opd_id is '0':
        wh=''
        a='select * from "MUSRENBANG".new_laporan_ajuan_program_kegiatan_opd '+ wh
    else:
        wh="where skpd_id="+opd_id
        a='select * from "MUSRENBANG".new_laporan_ajuan_program_kegiatan_opd '+ wh
    # return HttpResponse(a)
    cursor = connection.cursor()
    cursor.execute(a)
    Data = cursor.fetchall()
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Ajuan Program OPD.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Ajuan Program OPD', cell_overwrite_ok=True)
    # Sheet header, first row
    row_num = 0
    # borders
    borders = xlwt.Borders()
    borders.left = 1
    borders.right = 1
    borders.top = 1
    borders.bottom = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.borders = borders

    columns = ['No', 'Kode OPD', 'OPD', 'Kode Porgram','Program', 'Kegiatan','Sub Kegiatan','Anggaran' ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Format Currency
    style2 = xlwt.XFStyle()
    style2.num_format_str = '$#,##0.00'
    style2.borders = borders
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.borders = borders

    style2 = xlwt.easyxf('font: bold off, color black;\
                             align: horiz right')
    # cursor = connection.cursor()
    # cursor.execute('SELECT "REFERENSI"."REF_SKPD"."SKPD_KODE" skpd_kode,"REFERENSI"."REF_SKPD"."SKPD_NAMA" skpd_nama,program,kegiatan,sub_kegiatan,anggaran,apbd,dbhct,dak,tp,lainya FROM "MUSRENBANG".rekap_program_kegiatan RIGHT JOIN "REFERENSI"."REF_SKPD" ON "MUSRENBANG".rekap_program_kegiatan."SKPD_ID" = "REFERENSI"."REF_SKPD"."SKPD_ID" ORDER BY  skpd_kode asc')
    # # cursor.execute('select * from "REFERENSI"."REF_SKPD"')
    # Data = cursor.fetchall()
    print(Data)
    # for row in Data:
    for name, num in enumerate(Data):
        row_num += 1
        ws.write(row_num, 0, row_num, font_style)
        ws.write(row_num, 1, "{1}".format(name, num[1]) , font_style)
        ws.write(row_num, 2, "{1}".format(name, num[2]) , font_style)
        ws.write(row_num, 3, "{1}".format(name, num[3]) , font_style)
        ws.write(row_num, 4, "{1}".format(name, num[4]) , style2)
        ws.write(row_num, 5, "{1}".format(name, num[5]) , style2)
        ws.write(row_num, 6, "{1}".format(name, num[6]) , style2)
        ws.write(row_num, 7, "{1}".format(name, num[7]) , style2)

    wb.save(response)
    return response
def rekapAjuanKegiatanopd(request,opd_id=None):
    if opd_id is None:
        a='0'
        wh="where skpd_id="+a
        sql='select * from "MUSRENBANG".new_ajuan_kegiatan_opd '+wh+' order by  skpd_kode asc,program_kode asc, kegiatan_kode asc'
    else:
        wh="where skpd_id="+opd_id
        sql='select * from "MUSRENBANG".new_ajuan_kegiatan_opd '+wh+' order by  skpd_kode asc,program_kode asc, kegiatan_kode asc'

    cursor = connection.cursor()
    cursor.execute(sql)
    Data = cursor.fetchall()
    cursor = connection.cursor()
    cursor.execute('select "SKPD_ID","SKPD_NAMA" from "REFERENSI"."REF_SKPD"')
    Opd = cursor.fetchall()
    context = {
        'title'          : 'Ajuan Kegiatan OPD',
        'active_rekapAjuanKegiatanopd'    : 'active',
        'Data'           : Data,
        'Opd'           : Opd,
        'Opd_id'           : opd_id,
    }
    return render(request,'monitoring/rekapAjuanKegiatanopd.html',context)
def excel_view_rekapAjuanKegiatanopd(request,opd_id=None):
    if opd_id is '0':
        wh=''
        a='select * from "MUSRENBANG".new_laporan_ajuan_program_kegiatan_opd '+ wh
    else:
        wh="where skpd_id="+opd_id
        a='select * from "MUSRENBANG".new_ajuan_kegiatan_opd '+wh+' order by  skpd_kode asc,program_kode asc, kegiatan_kode asc'
    # return HttpResponse(a)
    cursor = connection.cursor()
    cursor.execute(a)
    Data = cursor.fetchall()
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Ajuan Kegiatan OPD.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Ajuan Kegiatan OPD', cell_overwrite_ok=True)
    # Sheet header, first row
    row_num = 0
    # borders
    borders = xlwt.Borders()
    borders.left = 1
    borders.right = 1
    borders.top = 1
    borders.bottom = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.borders = borders

    columns = ['No', 'Kode OPD', 'OPD', 'Kode Porgram','Program','Kode Kegiatan', 'Kegiatan','Sub Kegiatan','APBD', 'DBHCHT', 'DAK', 'TP', 'LAINNYA' ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Format Currency
    style2 = xlwt.XFStyle()
    style2.num_format_str = '$#,##0.00'
    style2.borders = borders
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.borders = borders

    style2 = xlwt.easyxf('font: bold off, color black;\
                             align: horiz right')
    # cursor = connection.cursor()
    # cursor.execute('SELECT "REFERENSI"."REF_SKPD"."SKPD_KODE" skpd_kode,"REFERENSI"."REF_SKPD"."SKPD_NAMA" skpd_nama,program,kegiatan,sub_kegiatan,anggaran,apbd,dbhct,dak,tp,lainya FROM "MUSRENBANG".rekap_program_kegiatan RIGHT JOIN "REFERENSI"."REF_SKPD" ON "MUSRENBANG".rekap_program_kegiatan."SKPD_ID" = "REFERENSI"."REF_SKPD"."SKPD_ID" ORDER BY  skpd_kode asc')
    # # cursor.execute('select * from "REFERENSI"."REF_SKPD"')
    # Data = cursor.fetchall()
    print(Data)
    # for row in Data:
    for name, num in enumerate(Data):
        row_num += 1
        ws.write(row_num, 0, row_num, font_style)
        ws.write(row_num, 1, "{1}".format(name, num[2]) , font_style)
        ws.write(row_num, 2, "{1}".format(name, num[3]) , font_style)
        ws.write(row_num, 3, "{1}".format(name, num[6]) , font_style)
        ws.write(row_num, 4, "{1}".format(name, num[7]) , font_style)
        ws.write(row_num, 5, "{1}".format(name, num[8]) , font_style)
        ws.write(row_num, 6, "{1}".format(name, num[9]) , font_style)
        ws.write(row_num, 7, "{1}".format(name, num[10]) , font_style)
        ws.write(row_num, 8, "{1}".format(name, num[12]) , style2)
        ws.write(row_num, 9, "{1}".format(name, num[13]) , style2)
        ws.write(row_num, 10, "{1}".format(name, num[14]) , style2)
        ws.write(row_num, 11, "{1}".format(name, num[15]) , style2)
        ws.write(row_num, 12, "{1}".format(name, num[16]) , style2)

    wb.save(response)
    return response
def rekapAjuan(request):
    cursor = connection.cursor()
    cursor.execute('SELECT "REFERENSI"."REF_SKPD"."SKPD_KODE" skpd_kode,"REFERENSI"."REF_SKPD"."SKPD_NAMA" skpd_nama,program,kegiatan,sub_kegiatan,anggaran,apbd,dbhct,dak,tp,lainya FROM "MUSRENBANG".rekap_program_kegiatan RIGHT JOIN "REFERENSI"."REF_SKPD" ON "MUSRENBANG".rekap_program_kegiatan."SKPD_ID" = "REFERENSI"."REF_SKPD"."SKPD_ID" ORDER BY  skpd_kode asc')
    # cursor.execute('select * from "REFERENSI"."REF_SKPD"')
    Data = cursor.fetchall()
    # print(posts)
    context = {
        'title'          : 'Rekapitulasi Ajuan',
        'active_rekapAjuan'    : 'active',
        'Data'           : Data,
    }
    # print(Data)
    return render(request,'monitoring/rekapAjuan.html',context)
def excel_view(request):

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Rekapitulasi Ajuan.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Rekapitulasi Ajuan', cell_overwrite_ok=True)
    # Sheet header, first row
    row_num = 0
    # borders
    borders = xlwt.Borders()
    borders.left = 1
    borders.right = 1
    borders.top = 1
    borders.bottom = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.borders = borders

    columns = ['No', 'Kode OPD', 'OPD', 'Porgram', 'Kegiatan','Sub Kegiatan','APBD','DBHCHT','DAK','TP','LAINYA','Total Anggaran' ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Format Currency
    style2 = xlwt.XFStyle()
    style2.num_format_str = '$#,##0.00'
    style2.borders = borders
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.borders = borders

    style2 = xlwt.easyxf('font: bold off, color black;\
                             align: horiz right')
    cursor = connection.cursor()
    cursor.execute('SELECT "REFERENSI"."REF_SKPD"."SKPD_KODE" skpd_kode,"REFERENSI"."REF_SKPD"."SKPD_NAMA" skpd_nama,program,kegiatan,sub_kegiatan,anggaran,apbd,dbhct,dak,tp,lainya FROM "MUSRENBANG".rekap_program_kegiatan RIGHT JOIN "REFERENSI"."REF_SKPD" ON "MUSRENBANG".rekap_program_kegiatan."SKPD_ID" = "REFERENSI"."REF_SKPD"."SKPD_ID" ORDER BY  skpd_kode asc')
    # cursor.execute('select * from "REFERENSI"."REF_SKPD"')
    Data = cursor.fetchall()
    print(Data)
    # for row in Data:
    for name, num in enumerate(Data):
        row_num += 1
        ws.write(row_num, 0, row_num, font_style)
        ws.write(row_num, 1, "{1}".format(name, num[0]) , font_style)
        ws.write(row_num, 2, "{1}".format(name, num[1]) , font_style)
        ws.write(row_num, 3, "{1}".format(name, num[2]) , font_style)
        ws.write(row_num, 4, "{1}".format(name, num[3]) , style2)
        ws.write(row_num, 5, "{1}".format(name, num[4]) , style2)
        ws.write(row_num, 6, "{1}".format(name, num[6]) , style2)
        ws.write(row_num, 7, "{1}".format(name, num[7]) , style2)
        ws.write(row_num, 8, "{1}".format(name, num[8]) , style2)
        ws.write(row_num, 9, "{1}".format(name, num[9]) , style2)
        ws.write(row_num, 10, "{1}".format(name, num[10]) , style2)
        ws.write(row_num, 11, "{1}".format(name, num[5]) , style2)

    wb.save(response)
    return response
def renja2(request,opd_id=None):
    if opd_id is None:
        a='0'
        sql='SELECT * FROM "MUSRENBANG"."LapRenja2" WHERE "SKPD_ID"='+a
    else:
        sql='SELECT * FROM "MUSRENBANG"."LapRenja2" WHERE "SKPD_ID"='+opd_id

    cursor = connection.cursor()
    cursor.execute(sql)
    Data = cursor.fetchall()
    cursor = connection.cursor()
    cursor.execute('select "SKPD_ID","SKPD_NAMA" from "REFERENSI"."REF_SKPD"')
    Opd = cursor.fetchall()
    context = {
        'title'          : 'Laporan Renja 2',
        'active_renja2'    : 'active',
        'Data'           : Data,
        'Opd'           : Opd,
        'Opd_id'           : opd_id,
    }
    return render(request,'monitoring/renja2.html',context)
def excel_view_renja2(request,opd_id=None):
    if opd_id is '0':
        wh=''
        a='select * from "MUSRENBANG".new_laporan_ajuan_program_kegiatan_opd '+ wh
    else:
        wh="where skpd_id="+opd_id
        a='select * from "MUSRENBANG".new_ajuan_kegiatan_opd '+wh+' order by  skpd_kode asc,program_kode asc, kegiatan_kode asc'
    # return HttpResponse(a)
    cursor = connection.cursor()
    cursor.execute(a)
    Data = cursor.fetchall()
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Laporan Renja 2.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Laporan Renja 2', cell_overwrite_ok=True)
    # Sheet header, first row
    row_num = 0
    # borders
    borders = xlwt.Borders()
    borders.left = 1
    borders.right = 1
    borders.top = 1
    borders.bottom = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.borders = borders

    columns = ['No', 'Kode OPD', 'OPD', 'Kode Porgram','Program','Kode Kegiatan', 'Kegiatan','Sub Kegiatan','APBD', 'DBHCHT', 'DAK', 'TP', 'LAINNYA' ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Format Currency
    style2 = xlwt.XFStyle()
    style2.num_format_str = '$#,##0.00'
    style2.borders = borders
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.borders = borders

    style2 = xlwt.easyxf('font: bold off, color black;\
                             align: horiz right')
    # cursor = connection.cursor()
    # cursor.execute('SELECT "REFERENSI"."REF_SKPD"."SKPD_KODE" skpd_kode,"REFERENSI"."REF_SKPD"."SKPD_NAMA" skpd_nama,program,kegiatan,sub_kegiatan,anggaran,apbd,dbhct,dak,tp,lainya FROM "MUSRENBANG".rekap_program_kegiatan RIGHT JOIN "REFERENSI"."REF_SKPD" ON "MUSRENBANG".rekap_program_kegiatan."SKPD_ID" = "REFERENSI"."REF_SKPD"."SKPD_ID" ORDER BY  skpd_kode asc')
    # # cursor.execute('select * from "REFERENSI"."REF_SKPD"')
    # Data = cursor.fetchall()
    print(Data)
    # for row in Data:
    for name, num in enumerate(Data):
        row_num += 1
        ws.write(row_num, 0, row_num, font_style)
        ws.write(row_num, 1, "{1}".format(name, num[2]) , font_style)
        ws.write(row_num, 2, "{1}".format(name, num[3]) , font_style)
        ws.write(row_num, 3, "{1}".format(name, num[6]) , font_style)
        ws.write(row_num, 4, "{1}".format(name, num[7]) , font_style)
        ws.write(row_num, 5, "{1}".format(name, num[8]) , font_style)
        ws.write(row_num, 6, "{1}".format(name, num[9]) , font_style)
        ws.write(row_num, 7, "{1}".format(name, num[10]) , font_style)
        ws.write(row_num, 8, "{1}".format(name, num[12]) , style2)
        ws.write(row_num, 9, "{1}".format(name, num[13]) , style2)
        ws.write(row_num, 10, "{1}".format(name, num[14]) , style2)
        ws.write(row_num, 11, "{1}".format(name, num[15]) , style2)
        ws.write(row_num, 12, "{1}".format(name, num[16]) , style2)

    wb.save(response)
    return response
def kegiatanEntry(request,opd_id=None):
    if opd_id is None:
        wh="WHERE created_at > '2019-03-12' "
    else:
        a="WHERE created_at > '2019-03-12' "
        wh=a+' and a."SKPD_ID"='+opd_id

    cursor = connection.cursor()
    cursor.execute('select a.*,b."SKPD_KODE", b."SKPD_NAMA" from "MUSRENBANG".data_kegiatan3 a join "REFERENSI"."REF_SKPD" b on a."SKPD_ID"=b."SKPD_ID" '+ wh)
    Data = cursor.fetchall()
    cursor = connection.cursor()
    cursor.execute('select "SKPD_ID","SKPD_NAMA" from "REFERENSI"."REF_SKPD"')
    Opd = cursor.fetchall()
    context = {
        'title'          : 'Kegiatan yg dientry Setelah Forum OPD',
        'active_kegiatanEntry'    : 'active',
        'Data'           : Data,
        'Opd'           : Opd,
        'Opd_id'           : opd_id,
    }
    return render(request,'monitoring/kegiatanEntry.html',context)
def excel_view_kegiatanEntry(request,opd_id=None):

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Kegiatan yg dientry Setelah Forum OPD.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Entry Setelah Forum OPD', cell_overwrite_ok=True)
    # Sheet header, first row
    row_num = 0
    # borders
    borders = xlwt.Borders()
    borders.left = 1
    borders.right = 1
    borders.top = 1
    borders.bottom = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    font_style.borders = borders

    columns = ['No', 'Kode OPD', 'OPD', 'Kode Program', 'Program','Kode Kegiatan','Kegiatan','Sub Kegiatan','Anggaran' ]
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Format Currency
    style2 = xlwt.XFStyle()
    style2.num_format_str = '$#,##0.00'
    style2.borders = borders
    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.borders = borders

    style2 = xlwt.easyxf('font: bold off, color black;\
                             align: horiz right')

    if opd_id is None:
        wh="WHERE created_at > '2019-03-12' "
    else:
        a="WHERE created_at > '2019-03-12' "
        wh=a+' and a."SKPD_ID"='+opd_id

    cursor = connection.cursor()
    cursor.execute('select a.*,b."SKPD_KODE", b."SKPD_NAMA" from "MUSRENBANG".data_kegiatan3 a join "REFERENSI"."REF_SKPD" b on a."SKPD_ID"=b."SKPD_ID" '+ wh)
    Data = cursor.fetchall()
    print(Data)
    # for row in Data:
    for name, num in enumerate(Data):
        row_num += 1
        ws.write(row_num, 0, row_num, font_style)
        ws.write(row_num, 1, "{1}".format(name, num[16]) , font_style)
        ws.write(row_num, 2, "{1}".format(name, num[17]) , font_style)
        ws.write(row_num, 3, "{1}".format(name, num[5]) , font_style)
        ws.write(row_num, 4, "{1}".format(name, num[6]) , font_style)
        ws.write(row_num, 5, "{1}".format(name, num[2]) , font_style)
        ws.write(row_num, 6, "{1}".format(name, num[3]) , font_style)
        ws.write(row_num, 7, "{1}".format(name, num[9]) , style2)
        ws.write(row_num, 8, "{1}".format(name, num[10]) , style2)

    wb.save(response)
    return response