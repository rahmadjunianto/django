from django.shortcuts import render
from django.db import connection

def index(request):
    cursor = connection.cursor()
    cursor.execute('SELECT id FROM "DATA".users where level= %s', [5])
    User = cursor.fetchall()
    context = {
        'title'             : 'Dashboard',
        'heading'           : 'Selamat Datang',
        'subheading'        : 'di kelas terbuka',
        'active_dashboard'  : 'active',
        'User'              : User
    }
    print(User)
    return render(request,'index.html', context)

def login(request):
    return render(request,'login.html')