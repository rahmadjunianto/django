from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^monitoring/', include('monitoring.urls', namespace='monitoring')),
    url(r'^grafik/', include('grafik.urls', namespace='grafik')),
    url(r'^about/', include('about.urls')),
    url(r'^$', views.index, name="index"),
    url(r'^login/$', views.login, name="login")
]
