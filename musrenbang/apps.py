from django.apps import AppConfig


class MusrenbangConfig(AppConfig):
    name = 'musrenbang'
