# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class DatBerita(models.Model):
    tgl = models.DateField(db_column='TGL', blank=True, null=True)  # Field name made lowercase.
    id = models.IntegerField(blank=True, null=True)
    berita_id = models.AutoField(db_column='BERITA_ID', primary_key=True)  # Field name made lowercase.
    lampiran = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_BERITA'


class DatBeritaFoto(models.Model):
    id_berita_foto = models.AutoField(db_column='ID_BERITA_FOTO', primary_key=True)  # Field name made lowercase.
    tgl_update = models.DateField(blank=True, null=True)
    image = models.TextField(blank=True, null=True)
    id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_BERITA_FOTO'


class DatDetailUsulan(models.Model):
    detail_id = models.AutoField(db_column='DETAIL_ID', primary_key=True)  # Field name made lowercase.
    rt_id = models.SmallIntegerField(db_column='RT_ID', blank=True, null=True)  # Field name made lowercase.
    usulan_id = models.SmallIntegerField(db_column='USULAN_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_DETAIL_USULAN'


class DatIndikatorSasaranKegiatan(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    indikator = models.TextField(db_column='INDIKATOR')  # Field name made lowercase.
    dat_sasaran_kegiatan = models.ForeignKey('DatSasaranKegiatan', models.DO_NOTHING, db_column='DAT_SASARAN_KEGIATAN_ID', blank=True, null=True)  # Field name made lowercase.
    target1 = models.FloatField(db_column='TARGET1', blank=True, null=True)  # Field name made lowercase.
    satuan1 = models.CharField(db_column='SATUAN1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sub_skpd_bidang_id = models.IntegerField(db_column='SUB_SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    tahun1 = models.IntegerField(db_column='TAHUN1', blank=True, null=True)  # Field name made lowercase.
    lokasi1 = models.CharField(db_column='LOKASI1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anggaran1 = models.BigIntegerField(db_column='ANGGARAN1', blank=True, null=True)  # Field name made lowercase.
    sumber_pendanaan = models.CharField(db_column='SUMBER_PENDANAAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tahun2 = models.CharField(db_column='TAHUN2', max_length=4, blank=True, null=True)  # Field name made lowercase.
    target2 = models.IntegerField(db_column='TARGET2', blank=True, null=True)  # Field name made lowercase.
    satuan2 = models.CharField(db_column='SATUAN2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lokasi2 = models.CharField(db_column='LOKASI2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anggaran2 = models.IntegerField(db_column='ANGGARAN2', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_INDIKATOR_SASARAN_KEGIATAN'


class DatIndikatorSasaranKegiatanCopy1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    indikator = models.TextField(db_column='INDIKATOR')  # Field name made lowercase.
    dat_sasaran_kegiatan_id = models.IntegerField(db_column='DAT_SASARAN_KEGIATAN_ID', blank=True, null=True)  # Field name made lowercase.
    target1 = models.FloatField(db_column='TARGET1', blank=True, null=True)  # Field name made lowercase.
    satuan1 = models.CharField(db_column='SATUAN1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sub_skpd_bidang_id = models.IntegerField(db_column='SUB_SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    tahun1 = models.IntegerField(db_column='TAHUN1', blank=True, null=True)  # Field name made lowercase.
    lokasi1 = models.CharField(db_column='LOKASI1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anggaran1 = models.BigIntegerField(db_column='ANGGARAN1', blank=True, null=True)  # Field name made lowercase.
    sumber_pendanaan = models.CharField(db_column='SUMBER_PENDANAAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tahun2 = models.CharField(db_column='TAHUN2', max_length=4, blank=True, null=True)  # Field name made lowercase.
    target2 = models.IntegerField(db_column='TARGET2', blank=True, null=True)  # Field name made lowercase.
    satuan2 = models.CharField(db_column='SATUAN2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lokasi2 = models.CharField(db_column='LOKASI2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anggaran2 = models.IntegerField(db_column='ANGGARAN2', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_INDIKATOR_SASARAN_KEGIATAN_copy1'


class DatIndikatorSasaranOpd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    indikator_sasaran = models.CharField(db_column='INDIKATOR_SASARAN', max_length=-1)  # Field name made lowercase.
    sasaran_opd = models.ForeignKey('DatSasaranOpd', models.DO_NOTHING, db_column='SASARAN_OPD_ID', blank=True, null=True)  # Field name made lowercase.
    target = models.CharField(db_column='TARGET', max_length=100, blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_INDIKATOR_SASARAN_OPD'


class DatIndikatorSasaranProgram(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    indikator = models.TextField(db_column='INDIKATOR')  # Field name made lowercase.
    sasaran_program = models.ForeignKey('DatSasaranProgram', models.DO_NOTHING, db_column='SASARAN_PROGRAM_ID', blank=True, null=True)  # Field name made lowercase.
    target = models.FloatField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_bidang_id = models.IntegerField(db_column='SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.IntegerField(db_column='TAHUN', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_INDIKATOR_SASARAN_PROGRAM'


class DatIndikatorSasaranProgramCopy1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    indikator = models.TextField(db_column='INDIKATOR')  # Field name made lowercase.
    sasaran_program_id = models.IntegerField(db_column='SASARAN_PROGRAM_ID', blank=True, null=True)  # Field name made lowercase.
    target = models.IntegerField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_bidang_id = models.IntegerField(db_column='SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.IntegerField(db_column='TAHUN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_INDIKATOR_SASARAN_PROGRAM_copy1'


class DatKegiatan(models.Model):
    kegiatan_id = models.AutoField(db_column='KEGIATAN_ID', primary_key=True)  # Field name made lowercase.
    kegiatan_nama = models.CharField(db_column='KEGIATAN_NAMA', max_length=255)  # Field name made lowercase.
    kegiatan_rab = models.CharField(db_column='KEGIATAN_RAB', max_length=255, blank=True, null=True)  # Field name made lowercase.
    usulan_id = models.IntegerField(db_column='USULAN_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_KEGIATAN'


class DatKegiatan2(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kegiatan_id = models.IntegerField(db_column='KEGIATAN_ID')  # Field name made lowercase.
    sasaran = models.CharField(db_column='SASARAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    indikator = models.CharField(db_column='INDIKATOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lokasi = models.CharField(db_column='LOKASI', max_length=255, blank=True, null=True)  # Field name made lowercase.
    target_anggaran = models.BigIntegerField(db_column='TARGET_ANGGARAN', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    apbd = models.BigIntegerField(db_column='APBD', blank=True, null=True)  # Field name made lowercase.
    dak = models.BigIntegerField(db_column='DAK', blank=True, null=True)  # Field name made lowercase.
    tp = models.BigIntegerField(db_column='TP', blank=True, null=True)  # Field name made lowercase.
    dbchcht_field = models.BigIntegerField(db_column='DBCHCHT\r\n', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    lainnya = models.BigIntegerField(db_column='LAINNYA', blank=True, null=True)  # Field name made lowercase.
    anggaran = models.BigIntegerField(db_column='ANGGARAN', blank=True, null=True)  # Field name made lowercase.
    target2 = models.IntegerField(db_column='TARGET2', blank=True, null=True)  # Field name made lowercase.
    satuan2 = models.CharField(db_column='SATUAN2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    status_posisi = models.IntegerField(db_column='STATUS_POSISI', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    sub_kegiatan = models.TextField(db_column='SUB_KEGIATAN', blank=True, null=True)  # Field name made lowercase.
    dat_program_id = models.IntegerField(db_column='DAT_PROGRAM_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_bidang_id = models.IntegerField(db_column='SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_skpd_id = models.IntegerField(db_column='SUB_BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    kec_id = models.IntegerField(db_column='KEC_ID', blank=True, null=True)  # Field name made lowercase.
    kel_id = models.IntegerField(db_column='KEL_ID', blank=True, null=True)  # Field name made lowercase.
    usulan_id = models.IntegerField(db_column='USULAN_ID', blank=True, null=True)  # Field name made lowercase.
    volume = models.IntegerField(db_column='VOLUME', blank=True, null=True)  # Field name made lowercase.
    lokasi_detail = models.CharField(db_column='LOKASI_DETAIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    username_insert = models.CharField(db_column='USERNAME_INSERT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kegiatan_nama = models.CharField(db_column='KEGIATAN_NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    program_nama = models.CharField(db_column='PROGRAM_NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    catatan_kasubidbpd = models.TextField(db_column='CATATAN_KASUBIDBPD', blank=True, null=True)  # Field name made lowercase.
    catatan_kabidbpd = models.TextField(db_column='CATATAN_KABIDBPD', blank=True, null=True)  # Field name made lowercase.
    catatan_kasubidbpd2 = models.TextField(db_column='CATATAN_KASUBIDBPD2', blank=True, null=True)  # Field name made lowercase.
    catatan_kabidbpd2 = models.TextField(db_column='CATATAN_KABIDBPD2', blank=True, null=True)  # Field name made lowercase.
    tgl_status = models.DateField(db_column='TGL_STATUS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_KEGIATAN2'


class DatKegiatanDetail(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    target = models.FloatField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lokasi = models.TextField(db_column='LOKASI', blank=True, null=True)  # Field name made lowercase.
    anggaran = models.BigIntegerField(db_column='ANGGARAN', blank=True, null=True)  # Field name made lowercase.
    sumber_pendanaan = models.CharField(db_column='SUMBER_PENDANAAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dat_kegiatan2_id = models.IntegerField(db_column='DAT_KEGIATAN2_ID', blank=True, null=True)  # Field name made lowercase.
    sub_kegiatan = models.TextField(db_column='SUB_KEGIATAN', blank=True, null=True)  # Field name made lowercase.
    apbd = models.BigIntegerField(db_column='APBD', blank=True, null=True)  # Field name made lowercase.
    dbhct = models.BigIntegerField(db_column='DBHCT', blank=True, null=True)  # Field name made lowercase.
    dak = models.BigIntegerField(db_column='DAK', blank=True, null=True)  # Field name made lowercase.
    tp = models.BigIntegerField(db_column='TP', blank=True, null=True)  # Field name made lowercase.
    lainya = models.BigIntegerField(db_column='LAINYA', blank=True, null=True)  # Field name made lowercase.
    target2 = models.TextField(db_column='TARGET2', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_KEGIATAN_DETAIL'


class DatPaguBappeda(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    b_langsung = models.BigIntegerField(db_column='B_LANGSUNG', blank=True, null=True)  # Field name made lowercase.
    b_tak_langsung = models.BigIntegerField(db_column='B_TAK_LANGSUNG', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_PAGU_BAPPEDA'


class DatPaguBidangSkpd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nilai_pagu = models.BigIntegerField(db_column='NILAI_PAGU')  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    bidang_skpd_id = models.IntegerField(db_column='BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_PAGU_BIDANG_SKPD'


class DatPaguProgram(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nilai_pagu = models.BigIntegerField(db_column='NILAI_PAGU', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    program_id = models.IntegerField(db_column='PROGRAM_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_PAGU_PROGRAM'


class DatPaguSkpd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nilai_pagu = models.BigIntegerField(db_column='NILAI_PAGU')  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    naratif1 = models.TextField(db_column='NARATIF1', blank=True, null=True)  # Field name made lowercase.
    naratif2 = models.TextField(db_column='NARATIF2', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_PAGU_SKPD'


class DatPenerima(models.Model):
    penerima_id = models.AutoField(db_column='PENERIMA_ID', primary_key=True)  # Field name made lowercase.
    penerima_nama = models.CharField(db_column='PENERIMA_NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    usulan_id = models.IntegerField(db_column='USULAN_ID', blank=True, null=True)  # Field name made lowercase.
    penerima_lokasi = models.CharField(db_column='PENERIMA_LOKASI', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_nik = models.CharField(db_column='PENERIMA_NIK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_rab = models.IntegerField(db_column='PENERIMA_RAB', blank=True, null=True)  # Field name made lowercase.
    penerima_ketua = models.CharField(db_column='PENERIMA_KETUA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_sk = models.CharField(db_column='PENERIMA_SK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_volume = models.CharField(db_column='PENERIMA_VOLUME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_jenis_paud = models.CharField(db_column='PENERIMA_JENIS_PAUD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_jenis_usaha = models.CharField(db_column='PENERIMA_JENIS_USAHA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_jenis_sarpras = models.CharField(db_column='PENERIMA_JENIS_SARPRAS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_alamat = models.CharField(db_column='PENERIMA_ALAMAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_alat = models.CharField(db_column='PENERIMA_ALAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_jenis_pelatihan = models.CharField(db_column='PENERIMA_JENIS_PELATIHAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    penerima_manfaat = models.CharField(db_column='PENERIMA_MANFAAT', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_PENERIMA'


class DatPerencanaan(models.Model):
    perencanaan_id = models.IntegerField(db_column='PERENCANAAN_ID', primary_key=True)  # Field name made lowercase.
    perencanaan_tahun = models.CharField(db_column='PERENCANAAN_TAHUN', max_length=10, blank=True, null=True)  # Field name made lowercase.
    perencanaan_aktif = models.SmallIntegerField(db_column='PERENCANAAN_AKTIF', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_PERENCANAAN'


class DatProgram(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    program_id = models.IntegerField(db_column='PROGRAM_ID')  # Field name made lowercase.
    sasaran = models.CharField(db_column='SASARAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    indikator = models.CharField(db_column='INDIKATOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    target = models.BigIntegerField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    status_posisi = models.IntegerField(db_column='STATUS_POSISI', blank=True, null=True)  # Field name made lowercase.
    sasaran_opd_id = models.IntegerField(db_column='SASARAN_OPD_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_bidang_id = models.IntegerField(db_column='SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    target_kinerja = models.BigIntegerField(db_column='TARGET_KINERJA', blank=True, null=True)  # Field name made lowercase.
    target_anggaran = models.BigIntegerField(db_column='TARGET_ANGGARAN', blank=True, null=True)  # Field name made lowercase.
    program_nama = models.CharField(db_column='PROGRAM_NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_PROGRAM'


class DatProgramCopy1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    program_id = models.IntegerField(db_column='PROGRAM_ID')  # Field name made lowercase.
    sasaran = models.CharField(db_column='SASARAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    indikator = models.CharField(db_column='INDIKATOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    target = models.BigIntegerField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    status_posisi = models.IntegerField(db_column='STATUS_POSISI', blank=True, null=True)  # Field name made lowercase.
    sasaran_opd_id = models.IntegerField(db_column='SASARAN_OPD_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_bidang_id = models.IntegerField(db_column='SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    target_kinerja = models.IntegerField(db_column='TARGET_KINERJA', blank=True, null=True)  # Field name made lowercase.
    target_anggaran = models.IntegerField(db_column='TARGET_ANGGARAN', blank=True, null=True)  # Field name made lowercase.
    program_nama = models.CharField(db_column='PROGRAM_NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_PROGRAM_copy1'


class DatSasaranKegiatan(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sasaran = models.TextField(db_column='SASARAN')  # Field name made lowercase.
    dat_kegiatan2 = models.ForeignKey(DatKegiatan2, models.DO_NOTHING, db_column='DAT_KEGIATAN2_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_skpd_id = models.IntegerField(db_column='SUB_BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_SASARAN_KEGIATAN'


class DatSasaranKegiatanCopy1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sasaran = models.TextField(db_column='SASARAN')  # Field name made lowercase.
    dat_kegiatan2_id = models.IntegerField(db_column='DAT_KEGIATAN2_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_skpd_id = models.IntegerField(db_column='SUB_BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_SASARAN_KEGIATAN_copy1'


class DatSasaranOpd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    tujuan_opd = models.ForeignKey('DatTujuanOpd', models.DO_NOTHING, db_column='TUJUAN_OPD_ID')  # Field name made lowercase.
    sasaran_strategis = models.TextField(db_column='SASARAN_STRATEGIS', blank=True, null=True)  # Field name made lowercase.
    target = models.IntegerField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_SASARAN_OPD'


class DatSasaranOpdCopy1(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    tujuan_opd_id = models.IntegerField(db_column='TUJUAN_OPD_ID')  # Field name made lowercase.
    sasaran_strategis = models.TextField(db_column='SASARAN_STRATEGIS', blank=True, null=True)  # Field name made lowercase.
    target = models.IntegerField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_SASARAN_OPD_copy1'


class DatSasaranProgram(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sasaran = models.TextField(db_column='SASARAN')  # Field name made lowercase.
    dat_program_id = models.IntegerField(db_column='DAT_PROGRAM_ID', blank=True, null=True)  # Field name made lowercase.
    skpd_bidang_id = models.IntegerField(db_column='SKPD_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_SASARAN_PROGRAM'


class DatStat(models.Model):
    id = models.AutoField()
    tgl = models.DateField(db_column='TGL', blank=True, null=True)  # Field name made lowercase.
    jam = models.IntegerField(db_column='JAM', blank=True, null=True)  # Field name made lowercase.
    online = models.IntegerField(db_column='ONLINE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_STAT'


class DatSubKegiatan(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sub_kegiatan = models.TextField(db_column='SUB_KEGIATAN', blank=True, null=True)  # Field name made lowercase.
    dat_kegiatan2_id = models.IntegerField(db_column='DAT_KEGIATAN2_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_SUB_KEGIATAN'


class DatTahapan(models.Model):
    id_tahapan = models.SmallIntegerField(db_column='ID_TAHAPAN', primary_key=True)  # Field name made lowercase.
    tgl_awal = models.DateField(db_column='TGL_AWAL')  # Field name made lowercase.
    tgl_akhir = models.DateField(db_column='TGL_AKHIR')  # Field name made lowercase.
    menu = models.CharField(db_column='MENU', max_length=2)  # Field name made lowercase.
    ket = models.CharField(db_column='KET', max_length=100)  # Field name made lowercase.
    kec_id = models.SmallIntegerField(db_column='KEC_ID', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_TAHAPAN'


class DatTahapanRenja(models.Model):
    tahapan_renja_id = models.AutoField(db_column='TAHAPAN_RENJA_ID', primary_key=True)  # Field name made lowercase.
    tgl_awal = models.DateField(db_column='TGL_AWAL')  # Field name made lowercase.
    tgl_akhir = models.DateField(db_column='TGL_AKHIR', blank=True, null=True)  # Field name made lowercase.
    menu = models.IntegerField(db_column='MENU', blank=True, null=True)  # Field name made lowercase.
    ket = models.CharField(db_column='KET', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    bidang_skpd_id = models.IntegerField(db_column='BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_skpd_id = models.IntegerField(db_column='SUB_BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    sub_bidang_id = models.IntegerField(db_column='SUB_BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    bidang_id = models.IntegerField(db_column='BIDANG_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_TAHAPAN_RENJA'


class DatTujuanOpd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    tujuan = models.TextField(db_column='TUJUAN')  # Field name made lowercase.
    indikator_tujuan = models.TextField(db_column='INDIKATOR_TUJUAN', blank=True, null=True)  # Field name made lowercase.
    target = models.IntegerField(db_column='TARGET', blank=True, null=True)  # Field name made lowercase.
    satuan = models.CharField(db_column='SATUAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    tahun = models.CharField(db_column='TAHUN', max_length=4, blank=True, null=True)  # Field name made lowercase.
    t2019 = models.FloatField(db_column='T2019', blank=True, null=True)  # Field name made lowercase.
    t2020 = models.FloatField(db_column='T2020', blank=True, null=True)  # Field name made lowercase.
    t2021 = models.FloatField(db_column='T2021', blank=True, null=True)  # Field name made lowercase.
    t2022 = models.FloatField(db_column='T2022', blank=True, null=True)  # Field name made lowercase.
    t2023 = models.FloatField(db_column='T2023', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_TUJUAN_OPD'


class DatUsulan(models.Model):
    usulan_id = models.AutoField(db_column='USULAN_ID', primary_key=True)  # Field name made lowercase.
    usulan_tahun = models.IntegerField(db_column='USULAN_TAHUN')  # Field name made lowercase.
    kamus_id = models.IntegerField(db_column='KAMUS_ID')  # Field name made lowercase.
    usulan_urgensi = models.TextField(db_column='USULAN_URGENSI', blank=True, null=True)  # Field name made lowercase.
    usulan_volume = models.FloatField(db_column='USULAN_VOLUME', blank=True, null=True)  # Field name made lowercase.
    usulan_gambar = models.TextField(db_column='USULAN_GAMBAR', blank=True, null=True)  # Field name made lowercase.
    usulan_lat = models.CharField(db_column='USULAN_LAT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_long = models.CharField(db_column='USULAN_LONG', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_prioritas = models.IntegerField(db_column='USULAN_PRIORITAS', blank=True, null=True)  # Field name made lowercase.
    usulan_status = models.IntegerField(db_column='USULAN_STATUS')  # Field name made lowercase.
    usulan_posisi = models.IntegerField(db_column='USULAN_POSISI')  # Field name made lowercase.
    rt_id = models.TextField(db_column='RT_ID', blank=True, null=True)  # Field name made lowercase.
    time_created = models.DateTimeField(db_column='TIME_CREATED', blank=True, null=True)  # Field name made lowercase.
    ip_created = models.CharField(db_column='IP_CREATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    user_updated = models.IntegerField(db_column='USER_UPDATED', blank=True, null=True)  # Field name made lowercase.
    time_updated = models.TimeField(db_column='TIME_UPDATED', blank=True, null=True)  # Field name made lowercase.
    ip_updated = models.CharField(db_column='IP_UPDATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    usulan_tujuan = models.IntegerField(db_column='USULAN_TUJUAN', blank=True, null=True)  # Field name made lowercase.
    usulan_alasan = models.TextField(db_column='USULAN_ALASAN', blank=True, null=True)  # Field name made lowercase.
    berita_acara = models.TextField(db_column='BERITA_ACARA', blank=True, null=True)  # Field name made lowercase.
    subtotal = models.FloatField(blank=True, null=True)
    user_created = models.SmallIntegerField(db_column='USER_CREATED', blank=True, null=True)  # Field name made lowercase.
    rw_id = models.SmallIntegerField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    alamat = models.TextField(db_column='ALAMAT', blank=True, null=True)  # Field name made lowercase.
    ket_acc = models.TextField(db_column='KET_ACC', blank=True, null=True)  # Field name made lowercase.
    usulan_catatankel = models.TextField(db_column='USULAN_CATATANKEL', blank=True, null=True)  # Field name made lowercase.
    usulan_catatankec = models.TextField(db_column='USULAN_CATATANKEC', blank=True, null=True)  # Field name made lowercase.
    usulan_catatanbidang = models.TextField(db_column='USULAN_CATATANBIDANG', blank=True, null=True)  # Field name made lowercase.
    usulan_catatansubbid = models.TextField(db_column='USULAN_CATATANSUBBID', blank=True, null=True)  # Field name made lowercase.
    usulan_idn = models.SmallIntegerField(db_column='USULAN_IDN', blank=True, null=True)  # Field name made lowercase.
    usulan_deleted = models.SmallIntegerField(db_column='USULAN_DELETED', blank=True, null=True)  # Field name made lowercase.
    usulan_volume1 = models.FloatField(db_column='USULAN_VOLUME1', blank=True, null=True)  # Field name made lowercase.
    usulan_volume2 = models.FloatField(db_column='USULAN_VOLUME2', blank=True, null=True)  # Field name made lowercase.
    satuan_id = models.SmallIntegerField(db_column='SATUAN_ID', blank=True, null=True)  # Field name made lowercase.
    satuan_id1 = models.SmallIntegerField(db_column='SATUAN_ID1', blank=True, null=True)  # Field name made lowercase.
    no_sk = models.CharField(db_column='NO_SK', max_length=100, blank=True, null=True)  # Field name made lowercase.
    tempat_ibadah = models.CharField(db_column='TEMPAT_IBADAH', max_length=255, blank=True, null=True)  # Field name made lowercase.
    usulan_catatankec2 = models.TextField(db_column='USULAN_CATATANKEC2', blank=True, null=True)  # Field name made lowercase.
    usulan_catatanbidang2 = models.TextField(db_column='USULAN_CATATANBIDANG2', blank=True, null=True)  # Field name made lowercase.
    usulan_catatansubbid2 = models.TextField(db_column='USULAN_CATATANSUBBID2', blank=True, null=True)  # Field name made lowercase.
    usulan_qty = models.IntegerField(db_column='USULAN_QTY', blank=True, null=True)  # Field name made lowercase.
    usulan_jumlah = models.IntegerField(db_column='USULAN_JUMLAH', blank=True, null=True)  # Field name made lowercase.
    usulan_harga = models.IntegerField(db_column='USULAN_HARGA', blank=True, null=True)  # Field name made lowercase.
    isu_id = models.IntegerField(db_column='ISU_ID', blank=True, null=True)  # Field name made lowercase.
    bidang_id = models.IntegerField(db_column='BIDANG_ID', blank=True, null=True)  # Field name made lowercase.
    usulan_catatanopd = models.CharField(db_column='USULAN_CATATANOPD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    usulan_catatanopd2 = models.CharField(db_column='USULAN_CATATANOPD2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dat_kegiatan2_id = models.IntegerField(db_column='DAT_KEGIATAN2_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DAT_USULAN'


class DatUsulanLikes(models.Model):
    oauth_uid = models.CharField(max_length=255)
    usulan_id = models.IntegerField(db_column='USULAN_ID', blank=True, null=True)  # Field name made lowercase.
    is_like = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DAT_USULAN_LIKES'


class PengecualianSkpd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PENGECUALIAN_SKPD'


class RkpUsulan(models.Model):
    usulan_id = models.AutoField(db_column='USULAN_ID', primary_key=True)  # Field name made lowercase.
    usulan_tahun = models.IntegerField(db_column='USULAN_TAHUN')  # Field name made lowercase.
    kamus_id = models.IntegerField(db_column='KAMUS_ID')  # Field name made lowercase.
    usulan_urgensi = models.TextField(db_column='USULAN_URGENSI', blank=True, null=True)  # Field name made lowercase.
    usulan_volume = models.FloatField(db_column='USULAN_VOLUME', blank=True, null=True)  # Field name made lowercase.
    usulan_gambar = models.TextField(db_column='USULAN_GAMBAR', blank=True, null=True)  # Field name made lowercase.
    usulan_lat = models.CharField(db_column='USULAN_LAT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_long = models.CharField(db_column='USULAN_LONG', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_prioritas = models.IntegerField(db_column='USULAN_PRIORITAS')  # Field name made lowercase.
    usulan_status = models.IntegerField(db_column='USULAN_STATUS')  # Field name made lowercase.
    usulan_posisi = models.IntegerField(db_column='USULAN_POSISI')  # Field name made lowercase.
    rt_id = models.TextField(db_column='RT_ID', blank=True, null=True)  # Field name made lowercase.
    time_created = models.DateTimeField(db_column='TIME_CREATED', blank=True, null=True)  # Field name made lowercase.
    ip_created = models.CharField(db_column='IP_CREATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    user_updated = models.IntegerField(db_column='USER_UPDATED', blank=True, null=True)  # Field name made lowercase.
    time_updated = models.TimeField(db_column='TIME_UPDATED', blank=True, null=True)  # Field name made lowercase.
    ip_updated = models.CharField(db_column='IP_UPDATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    usulan_tujuan = models.IntegerField(db_column='USULAN_TUJUAN', blank=True, null=True)  # Field name made lowercase.
    usulan_alasan = models.TextField(db_column='USULAN_ALASAN', blank=True, null=True)  # Field name made lowercase.
    berita_acara = models.TextField(db_column='BERITA_ACARA', blank=True, null=True)  # Field name made lowercase.
    subtotal = models.FloatField(blank=True, null=True)
    user_created = models.SmallIntegerField(db_column='USER_CREATED', blank=True, null=True)  # Field name made lowercase.
    rw_id = models.TextField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    alamat = models.TextField(db_column='ALAMAT', blank=True, null=True)  # Field name made lowercase.
    rkp_id = models.AutoField(db_column='RKP_ID')  # Field name made lowercase.
    ket_acc = models.TextField(db_column='KET_ACC', blank=True, null=True)  # Field name made lowercase.
    usulan_catatankel = models.TextField(db_column='USULAN_CATATANKEL', blank=True, null=True)  # Field name made lowercase.
    usulan_catatankec = models.TextField(db_column='USULAN_CATATANKEC', blank=True, null=True)  # Field name made lowercase.
    usulan_catatanskpd = models.TextField(db_column='USULAN_CATATANSKPD', blank=True, null=True)  # Field name made lowercase.
    usulan_catatanbappeda = models.TextField(db_column='USULAN_CATATANBAPPEDA', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RKP_USULAN'


class RkpUsulanKab(models.Model):
    usulan_id = models.IntegerField(db_column='USULAN_ID', primary_key=True)  # Field name made lowercase.
    usulan_tahun = models.IntegerField(db_column='USULAN_TAHUN')  # Field name made lowercase.
    kamus_id = models.IntegerField(db_column='KAMUS_ID')  # Field name made lowercase.
    usulan_urgensi = models.TextField(db_column='USULAN_URGENSI', blank=True, null=True)  # Field name made lowercase.
    usulan_volume = models.FloatField(db_column='USULAN_VOLUME')  # Field name made lowercase.
    usulan_gambar = models.TextField(db_column='USULAN_GAMBAR', blank=True, null=True)  # Field name made lowercase.
    usulan_lat = models.CharField(db_column='USULAN_LAT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_long = models.CharField(db_column='USULAN_LONG', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_prioritas = models.IntegerField(db_column='USULAN_PRIORITAS')  # Field name made lowercase.
    usulan_status = models.IntegerField(db_column='USULAN_STATUS')  # Field name made lowercase.
    usulan_posisi = models.IntegerField(db_column='USULAN_POSISI')  # Field name made lowercase.
    rt_id = models.TextField(db_column='RT_ID', blank=True, null=True)  # Field name made lowercase.
    time_created = models.DateTimeField(db_column='TIME_CREATED', blank=True, null=True)  # Field name made lowercase.
    ip_created = models.CharField(db_column='IP_CREATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    user_updated = models.IntegerField(db_column='USER_UPDATED', blank=True, null=True)  # Field name made lowercase.
    time_updated = models.TimeField(db_column='TIME_UPDATED', blank=True, null=True)  # Field name made lowercase.
    ip_updated = models.CharField(db_column='IP_UPDATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    usulan_tujuan = models.IntegerField(db_column='USULAN_TUJUAN', blank=True, null=True)  # Field name made lowercase.
    usulan_alasan = models.TextField(db_column='USULAN_ALASAN', blank=True, null=True)  # Field name made lowercase.
    berita_acara = models.TextField(db_column='BERITA_ACARA', blank=True, null=True)  # Field name made lowercase.
    subtotal = models.FloatField(blank=True, null=True)
    user_created = models.SmallIntegerField(db_column='USER_CREATED', blank=True, null=True)  # Field name made lowercase.
    rw_id = models.TextField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    alamat = models.TextField(db_column='ALAMAT', blank=True, null=True)  # Field name made lowercase.
    rkp_id_kec = models.AutoField(db_column='RKP_ID_KEC')  # Field name made lowercase.
    ket_acc = models.TextField(db_column='KET_ACC', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RKP_USULAN_KAB'
        unique_together = (('usulan_id', 'rkp_id_kec'),)


class RkpUsulanKec(models.Model):
    usulan_id = models.IntegerField(db_column='USULAN_ID', primary_key=True)  # Field name made lowercase.
    usulan_tahun = models.IntegerField(db_column='USULAN_TAHUN')  # Field name made lowercase.
    kamus_id = models.IntegerField(db_column='KAMUS_ID')  # Field name made lowercase.
    usulan_urgensi = models.TextField(db_column='USULAN_URGENSI', blank=True, null=True)  # Field name made lowercase.
    usulan_volume = models.FloatField(db_column='USULAN_VOLUME')  # Field name made lowercase.
    usulan_gambar = models.TextField(db_column='USULAN_GAMBAR', blank=True, null=True)  # Field name made lowercase.
    usulan_lat = models.CharField(db_column='USULAN_LAT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_long = models.CharField(db_column='USULAN_LONG', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_prioritas = models.IntegerField(db_column='USULAN_PRIORITAS')  # Field name made lowercase.
    usulan_status = models.IntegerField(db_column='USULAN_STATUS')  # Field name made lowercase.
    usulan_posisi = models.IntegerField(db_column='USULAN_POSISI')  # Field name made lowercase.
    rt_id = models.TextField(db_column='RT_ID', blank=True, null=True)  # Field name made lowercase.
    time_created = models.DateTimeField(db_column='TIME_CREATED', blank=True, null=True)  # Field name made lowercase.
    ip_created = models.CharField(db_column='IP_CREATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    user_updated = models.IntegerField(db_column='USER_UPDATED', blank=True, null=True)  # Field name made lowercase.
    time_updated = models.TimeField(db_column='TIME_UPDATED', blank=True, null=True)  # Field name made lowercase.
    ip_updated = models.CharField(db_column='IP_UPDATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    usulan_tujuan = models.IntegerField(db_column='USULAN_TUJUAN', blank=True, null=True)  # Field name made lowercase.
    usulan_alasan = models.TextField(db_column='USULAN_ALASAN', blank=True, null=True)  # Field name made lowercase.
    berita_acara = models.TextField(db_column='BERITA_ACARA', blank=True, null=True)  # Field name made lowercase.
    subtotal = models.FloatField(blank=True, null=True)
    user_created = models.SmallIntegerField(db_column='USER_CREATED', blank=True, null=True)  # Field name made lowercase.
    rw_id = models.TextField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    alamat = models.TextField(db_column='ALAMAT', blank=True, null=True)  # Field name made lowercase.
    rkp_id_kec = models.AutoField(db_column='RKP_ID_KEC')  # Field name made lowercase.
    ket_acc = models.TextField(db_column='KET_ACC', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RKP_USULAN_KEC'
        unique_together = (('usulan_id', 'rkp_id_kec'),)


class RkpUsulanSkpd(models.Model):
    usulan_id = models.IntegerField(db_column='USULAN_ID', primary_key=True)  # Field name made lowercase.
    usulan_tahun = models.IntegerField(db_column='USULAN_TAHUN')  # Field name made lowercase.
    kamus_id = models.IntegerField(db_column='KAMUS_ID')  # Field name made lowercase.
    usulan_urgensi = models.TextField(db_column='USULAN_URGENSI', blank=True, null=True)  # Field name made lowercase.
    usulan_volume = models.FloatField(db_column='USULAN_VOLUME')  # Field name made lowercase.
    usulan_gambar = models.TextField(db_column='USULAN_GAMBAR', blank=True, null=True)  # Field name made lowercase.
    usulan_lat = models.CharField(db_column='USULAN_LAT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_long = models.CharField(db_column='USULAN_LONG', max_length=100, blank=True, null=True)  # Field name made lowercase.
    usulan_prioritas = models.IntegerField(db_column='USULAN_PRIORITAS')  # Field name made lowercase.
    usulan_status = models.IntegerField(db_column='USULAN_STATUS')  # Field name made lowercase.
    usulan_posisi = models.IntegerField(db_column='USULAN_POSISI')  # Field name made lowercase.
    rt_id = models.TextField(db_column='RT_ID', blank=True, null=True)  # Field name made lowercase.
    time_created = models.DateTimeField(db_column='TIME_CREATED', blank=True, null=True)  # Field name made lowercase.
    ip_created = models.CharField(db_column='IP_CREATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    user_updated = models.IntegerField(db_column='USER_UPDATED', blank=True, null=True)  # Field name made lowercase.
    time_updated = models.TimeField(db_column='TIME_UPDATED', blank=True, null=True)  # Field name made lowercase.
    ip_updated = models.CharField(db_column='IP_UPDATED', max_length=20, blank=True, null=True)  # Field name made lowercase.
    usulan_tujuan = models.IntegerField(db_column='USULAN_TUJUAN', blank=True, null=True)  # Field name made lowercase.
    usulan_alasan = models.TextField(db_column='USULAN_ALASAN', blank=True, null=True)  # Field name made lowercase.
    berita_acara = models.TextField(db_column='BERITA_ACARA', blank=True, null=True)  # Field name made lowercase.
    subtotal = models.FloatField(blank=True, null=True)
    user_created = models.SmallIntegerField(db_column='USER_CREATED', blank=True, null=True)  # Field name made lowercase.
    rw_id = models.TextField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.
    alamat = models.TextField(db_column='ALAMAT', blank=True, null=True)  # Field name made lowercase.
    rkp_id_skpd = models.AutoField(db_column='RKP_ID_SKPD')  # Field name made lowercase.
    ket_acc = models.TextField(db_column='KET_ACC', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RKP_USULAN_SKPD'
        unique_together = (('usulan_id', 'rkp_id_skpd'),)


class SkpdStatus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    tgl_transaksi = models.DateField(db_column='TGL_TRANSAKSI', blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    catatan = models.TextField(db_column='CATATAN', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    status_posisi = models.IntegerField(db_column='STATUS_POSISI', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SKPD_STATUS'
