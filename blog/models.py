from django.db import models

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    category = models.CharField(max_length=20, blank=True)
    email = models.EmailField(default='nama@web.com')
    alamat = models.CharField(max_length=200, blank=True)
    waktu_posting = models.DateTimeField(auto_now_add=True)
    waktu_update = models.DateTimeField(auto_now= True)


    def __str__(self):
        return "{} . {}".format(self.id,self.title)