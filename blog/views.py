from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
# Create your views here.
from .models import Post
from .forms import PostForm
def index(request):

    posts= Post.objects.all()
    # print(posts)
    context = {
        'title'          : 'Blog',
        'active_blog'    : 'active',
        'posts'          : posts,
        'category'       : 'All'
    }
    return render(request,'blog/index.html',context)
def create(request):
    post_form = PostForm(request.POST or None)
    if request.method=='POST':
        Post.objects.create(
            title       = request.POST['title'],
            body        = request.POST['body'],
            category    = request.POST['category'],
        )
        return HttpResponseRedirect('/blog')
    else:
        pass
    context = {
        'title'          : 'Tambah',
        'active_blog'    : 'active',
        'post_form'      : post_form
    }
    return render(request,'blog/create.html',context)
def delete(request,delete_id):
    Post.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect('/blog')
def update(request,update_id):
    post = Post.objects.get(id=update_id)
    data = {
        'title' : post.title,
        'body'  : post.body,
        'category' : post.category
    }
    post_form = PostForm(request.POST or None, initial = data, instance=post)
    if request.method=='POST':
        post_form.save()
        return HttpResponseRedirect('/blog')
    else:
        pass
    context = {
        'title'          : 'Update',
        'active_blog'    : 'active',
        'post_form'      : post_form
    }
    return render(request,'blog/create.html',context)
def link(request,page):
    heading = "<h1>Page  {} </h1>".format(page)
    # str = heading + input
    return HttpResponse(heading)
def angka(request,input):
    heading = "<h1>Page Angka</h1>"
    str = heading + input
    return HttpResponse(str)
def tanggal(request,**input):
    heading = "<h1>Page Tanggal</h1>"
    strTahun = "tahun : " + input['tahun']
    strBulan = "Bulan : " + input['bulan']
    strHari = "Hari : " + input['hari']
    dateTanggal = "{}/{}/{}".format(input['tahun'],input['bulan'],input['hari'])
    str = heading + strTahun + "<br>" + strBulan + "<br>" +strHari
    return HttpResponse(heading + dateTanggal)
# def tanggal(request,tahun,bulan,hari):
#     heading = "<h1>Page Tanggal</h1>"
#     strTahun = "tahun : " + tahun
#     strBulan = "Bulan : " + bulan
#     strHari = "Hari : " + hari
#     str = heading + strTahun + "<br>" + strBulan + "<br>" +strHari
#     return HttpResponse(str)
def categoryPost(request,categoryInput):

    posts= Post.objects.filter(category=categoryInput)
    # print(posts)
    context = {
        'title'          : 'Blog',
        'heading'        :'ini adalah blog kelas terbuka',
        'subheading'     : '1 April 2019',
        'active_blog'    : 'active',
        'posts'          : posts,
        'category'       : categoryInput
    }
    return render(request,'blog/index.html',context)
def singlePost(request,postInput):

    posts= Post.objects.get(id=postInput)
    # print(posts)
    context = {
        'title'          : 'Blog',
        'heading'        :'ini adalah blog kelas terbuka',
        'subheading'     : '1 April 2019',
        'active_blog'    : 'active',
        'posts'          : posts,
        'category'       : postInput
    }
    return render(request,'blog/detail.html',context)
