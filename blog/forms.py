from django import forms

from .models import Post
# class PostForm(forms.Form):
#     title = forms.CharField(max_length= 20)
#     body = forms.CharField(
#         widget = forms.Textarea
#     )
#     category = forms.CharField(max_length= 20)

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            'title',
            'body',
            'category'
        ]