from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^delete/(?P<delete_id>[0-9])$', views.delete, name="delete"),
    url(r'^update/(?P<update_id>[0-9])$', views.update, name="update"),
	url(r'^category/(?P<categoryInput>[\w-]+)/$',views.categoryPost, name="category"),
	url(r'^post/(?P<postInput>[0-9])/$',views.singlePost, name="post"),
	url(r'^create/$',views.create,name="create"),
    # semua angka
    # url(r'^(?P<input>[0-9]+)/$', views.angka),
    # limit angka
    url(r'^(?P<input>[0-9]{2})/$', views.angka),
    url(r'^(?P<page>[\w]+)/$', views.link),
    url(r'^(?P<tahun>[0-9]{4})/(?P<bulan>[0-9]{2})/(?P<hari>[0-9]{2})/$', views.tanggal),
]