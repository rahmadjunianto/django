# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-04-01 07:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_post_waktu_posting'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='category',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
