# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class RefBidang(models.Model):
    bidang_id = models.AutoField(db_column='BIDANG_ID', primary_key=True)  # Field name made lowercase.
    bidang_nama = models.CharField(db_column='BIDANG_NAMA', max_length=-1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_BIDANG'


class RefBidangSkpd(models.Model):
    bidang_skpd_id = models.AutoField(db_column='BIDANG_SKPD_ID', primary_key=True)  # Field name made lowercase.
    bidang_skpd_nama = models.CharField(db_column='BIDANG_SKPD_NAMA', max_length=-1)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    jabatan = models.CharField(db_column='JABATAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nama = models.CharField(db_column='NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nip = models.CharField(db_column='NIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telp = models.CharField(db_column='TELP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pangkat = models.CharField(db_column='PANGKAT', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_BIDANG_SKPD'


class RefDesaPengecualian(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kel_id = models.CharField(db_column='KEL_ID', max_length=-1)  # Field name made lowercase.
    kec_id = models.CharField(db_column='KEC_ID', max_length=4, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_DESA_PENGECUALIAN'


class RefIsu(models.Model):
    isu_id = models.AutoField(db_column='ISU_ID', primary_key=True)  # Field name made lowercase.
    isu_tahun = models.IntegerField(db_column='ISU_TAHUN')  # Field name made lowercase.
    isu_nama = models.CharField(db_column='ISU_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    isu_kunci = models.IntegerField(db_column='ISU_KUNCI')  # Field name made lowercase.
    isu_tipe = models.IntegerField(db_column='ISU_TIPE', blank=True, null=True)  # Field name made lowercase.
    bidang_id = models.IntegerField(db_column='BIDANG_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_ISU'


class RefJenisKegiatan(models.Model):
    jenis_kegiatan_id = models.AutoField(db_column='JENIS_KEGIATAN_ID', primary_key=True)  # Field name made lowercase.
    jenis_kegiatan_nama = models.CharField(db_column='JENIS_KEGIATAN_NAMA', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_JENIS_KEGIATAN'


class RefJmlRlth(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kel_id = models.CharField(db_column='KEL_ID', max_length=4)  # Field name made lowercase.
    kec_id = models.CharField(db_column='KEC_ID', max_length=4, blank=True, null=True)  # Field name made lowercase.
    jumlah = models.IntegerField(db_column='JUMLAH', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_JML_RLTH'


class RefKamus(models.Model):
    kamus_id = models.IntegerField(db_column='KAMUS_ID', primary_key=True)  # Field name made lowercase.
    kamus_tahun = models.IntegerField(db_column='KAMUS_TAHUN', blank=True, null=True)  # Field name made lowercase.
    kamus_nama = models.CharField(db_column='KAMUS_NAMA', max_length=200)  # Field name made lowercase.
    kamus_kunci = models.IntegerField(db_column='KAMUS_KUNCI')  # Field name made lowercase.
    kamus_harga = models.FloatField(db_column='KAMUS_HARGA', blank=True, null=True)  # Field name made lowercase.
    kamus_satuan = models.CharField(db_column='KAMUS_SATUAN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    isu_id = models.IntegerField(db_column='ISU_ID', blank=True, null=True)  # Field name made lowercase.
    kamus_kriteria = models.TextField(db_column='KAMUS_KRITERIA', blank=True, null=True)  # Field name made lowercase.
    kamus_urusan = models.CharField(db_column='KAMUS_URUSAN', max_length=300, blank=True, null=True)  # Field name made lowercase.
    kamus_program = models.CharField(db_column='KAMUS_PROGRAM', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_kegiatan = models.CharField(db_column='KAMUS_KEGIATAN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_kode_komponen = models.CharField(db_column='KAMUS_KODE_KOMPONEN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_nama_komponen = models.CharField(db_column='KAMUS_NAMA_KOMPONEN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_operasional = models.TextField(db_column='KAMUS_OPERASIONAL', blank=True, null=True)  # Field name made lowercase.
    kamus_keterangan = models.TextField(db_column='KAMUS_KETERANGAN', blank=True, null=True)  # Field name made lowercase.
    kamus_jenis = models.CharField(db_column='KAMUS_JENIS', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kamus_skpd = models.IntegerField(db_column='KAMUS_SKPD', blank=True, null=True)  # Field name made lowercase.
    rekening_id = models.IntegerField(db_column='REKENING_ID', blank=True, null=True)  # Field name made lowercase.
    kegiatan_id = models.IntegerField(db_column='KEGIATAN_ID', blank=True, null=True)  # Field name made lowercase.
    kamus_ti = models.CharField(db_column='KAMUS_TI', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KAMUS'


class RefKamus2017(models.Model):
    kamus_id = models.IntegerField(db_column='KAMUS_ID', primary_key=True)  # Field name made lowercase.
    kamus_tahun = models.IntegerField(db_column='KAMUS_TAHUN', blank=True, null=True)  # Field name made lowercase.
    kamus_nama = models.CharField(db_column='KAMUS_NAMA', max_length=200)  # Field name made lowercase.
    kamus_kunci = models.IntegerField(db_column='KAMUS_KUNCI')  # Field name made lowercase.
    kamus_harga = models.FloatField(db_column='KAMUS_HARGA', blank=True, null=True)  # Field name made lowercase.
    kamus_satuan = models.CharField(db_column='KAMUS_SATUAN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    isu_id = models.IntegerField(db_column='ISU_ID', blank=True, null=True)  # Field name made lowercase.
    kamus_kriteria = models.TextField(db_column='KAMUS_KRITERIA', blank=True, null=True)  # Field name made lowercase.
    kamus_urusan = models.CharField(db_column='KAMUS_URUSAN', max_length=300, blank=True, null=True)  # Field name made lowercase.
    kamus_program = models.CharField(db_column='KAMUS_PROGRAM', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_kegiatan = models.CharField(db_column='KAMUS_KEGIATAN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_kode_komponen = models.CharField(db_column='KAMUS_KODE_KOMPONEN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_nama_komponen = models.CharField(db_column='KAMUS_NAMA_KOMPONEN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_operasional = models.TextField(db_column='KAMUS_OPERASIONAL', blank=True, null=True)  # Field name made lowercase.
    kamus_keterangan = models.TextField(db_column='KAMUS_KETERANGAN', blank=True, null=True)  # Field name made lowercase.
    kamus_jenis = models.CharField(db_column='KAMUS_JENIS', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kamus_skpd = models.IntegerField(db_column='KAMUS_SKPD', blank=True, null=True)  # Field name made lowercase.
    rekening_id = models.IntegerField(db_column='REKENING_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KAMUS_2017'


class RefKamusCopy1(models.Model):
    kamus_id = models.IntegerField(db_column='KAMUS_ID', primary_key=True)  # Field name made lowercase.
    kamus_tahun = models.IntegerField(db_column='KAMUS_TAHUN', blank=True, null=True)  # Field name made lowercase.
    kamus_nama = models.CharField(db_column='KAMUS_NAMA', max_length=200)  # Field name made lowercase.
    kamus_kunci = models.IntegerField(db_column='KAMUS_KUNCI')  # Field name made lowercase.
    kamus_harga = models.FloatField(db_column='KAMUS_HARGA', blank=True, null=True)  # Field name made lowercase.
    kamus_satuan = models.CharField(db_column='KAMUS_SATUAN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    isu_id = models.IntegerField(db_column='ISU_ID', blank=True, null=True)  # Field name made lowercase.
    kamus_kriteria = models.TextField(db_column='KAMUS_KRITERIA', blank=True, null=True)  # Field name made lowercase.
    kamus_urusan = models.CharField(db_column='KAMUS_URUSAN', max_length=300, blank=True, null=True)  # Field name made lowercase.
    kamus_program = models.CharField(db_column='KAMUS_PROGRAM', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_kegiatan = models.CharField(db_column='KAMUS_KEGIATAN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_kode_komponen = models.CharField(db_column='KAMUS_KODE_KOMPONEN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_nama_komponen = models.CharField(db_column='KAMUS_NAMA_KOMPONEN', max_length=200, blank=True, null=True)  # Field name made lowercase.
    kamus_operasional = models.TextField(db_column='KAMUS_OPERASIONAL', blank=True, null=True)  # Field name made lowercase.
    kamus_keterangan = models.TextField(db_column='KAMUS_KETERANGAN', blank=True, null=True)  # Field name made lowercase.
    kamus_jenis = models.CharField(db_column='KAMUS_JENIS', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kamus_skpd = models.IntegerField(db_column='KAMUS_SKPD', blank=True, null=True)  # Field name made lowercase.
    rekening_id = models.IntegerField(db_column='REKENING_ID', blank=True, null=True)  # Field name made lowercase.
    kegiatan_id = models.IntegerField(db_column='KEGIATAN_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KAMUS_copy1'


class RefKategoriPagu(models.Model):
    pagu_id = models.AutoField(db_column='PAGU_ID', primary_key=True)  # Field name made lowercase.
    pagu_nama = models.CharField(db_column='PAGU_NAMA', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KATEGORI_PAGU'


class RefKecamatan(models.Model):
    kec_id = models.AutoField(db_column='KEC_ID', primary_key=True)  # Field name made lowercase.
    kec_tahun = models.IntegerField(db_column='KEC_TAHUN')  # Field name made lowercase.
    kec_nama = models.CharField(db_column='KEC_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kec_camat = models.CharField(db_column='KEC_CAMAT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kec_nip = models.CharField(db_column='KEC_NIP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kec_telp = models.CharField(db_column='KEC_TELP', max_length=12, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KECAMATAN'


class RefKegiatan(models.Model):
    kegiatan_id = models.AutoField(db_column='KEGIATAN_ID', primary_key=True)  # Field name made lowercase.
    kegiatan_tahun = models.IntegerField(db_column='KEGIATAN_TAHUN', blank=True, null=True)  # Field name made lowercase.
    program_id = models.IntegerField(db_column='PROGRAM_ID', blank=True, null=True)  # Field name made lowercase.
    kegiatan_kode = models.CharField(db_column='KEGIATAN_KODE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    kegiatan_nama = models.CharField(db_column='KEGIATAN_NAMA', max_length=500, blank=True, null=True)  # Field name made lowercase.
    kegiatan_kunci = models.IntegerField(db_column='KEGIATAN_KUNCI', blank=True, null=True)  # Field name made lowercase.
    kegiatan_prioritas = models.IntegerField(db_column='KEGIATAN_PRIORITAS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KEGIATAN'


class RefKegiatanCopy1(models.Model):
    kegiatan_id = models.AutoField(db_column='KEGIATAN_ID', primary_key=True)  # Field name made lowercase.
    kegiatan_tahun = models.IntegerField(db_column='KEGIATAN_TAHUN', blank=True, null=True)  # Field name made lowercase.
    program_id = models.IntegerField(db_column='PROGRAM_ID', blank=True, null=True)  # Field name made lowercase.
    kegiatan_kode = models.CharField(db_column='KEGIATAN_KODE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    kegiatan_nama = models.CharField(db_column='KEGIATAN_NAMA', max_length=500, blank=True, null=True)  # Field name made lowercase.
    kegiatan_kunci = models.IntegerField(db_column='KEGIATAN_KUNCI', blank=True, null=True)  # Field name made lowercase.
    kegiatan_prioritas = models.IntegerField(db_column='KEGIATAN_PRIORITAS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KEGIATAN_copy1'


class RefKelurahan(models.Model):
    kel_id = models.AutoField(db_column='KEL_ID', primary_key=True)  # Field name made lowercase.
    kel_tahun = models.IntegerField(db_column='KEL_TAHUN')  # Field name made lowercase.
    kel_nama = models.CharField(db_column='KEL_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kel_lurah = models.CharField(db_column='KEL_LURAH', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kel_nip = models.CharField(db_column='KEL_NIP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    kel_telp = models.CharField(db_column='KEL_TELP', max_length=12, blank=True, null=True)  # Field name made lowercase.
    kec_id = models.IntegerField(db_column='KEC_ID')  # Field name made lowercase.
    tgl_awal = models.DateField(db_column='TGL_AWAL', blank=True, null=True)  # Field name made lowercase.
    tgl_akhir = models.DateField(db_column='TGL_AKHIR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KELURAHAN'
        unique_together = (('kel_id', 'kec_id'),)


class RefKelurahanCopy1(models.Model):
    kel_id = models.AutoField(db_column='KEL_ID', primary_key=True)  # Field name made lowercase.
    kel_tahun = models.IntegerField(db_column='KEL_TAHUN')  # Field name made lowercase.
    kel_nama = models.CharField(db_column='KEL_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kel_lurah = models.CharField(db_column='KEL_LURAH', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kel_nip = models.CharField(db_column='KEL_NIP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    kel_telp = models.CharField(db_column='KEL_TELP', max_length=12, blank=True, null=True)  # Field name made lowercase.
    kec_id = models.IntegerField(db_column='KEC_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KELURAHAN_copy1'
        unique_together = (('kel_id', 'kec_id'),)


class RefKetentuan(models.Model):
    ketentuan_id = models.IntegerField(db_column='KETENTUAN_ID', blank=True, null=True)  # Field name made lowercase.
    ketentuan_nama = models.TextField(db_column='KETENTUAN_NAMA')  # Field name made lowercase.
    isu_id = models.IntegerField(db_column='ISU_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KETENTUAN'


class RefKomponen(models.Model):
    komponen_id = models.AutoField(db_column='KOMPONEN_ID', primary_key=True)  # Field name made lowercase.
    komponen_tahun = models.IntegerField(db_column='KOMPONEN_TAHUN', blank=True, null=True)  # Field name made lowercase.
    komponen_kode = models.CharField(db_column='KOMPONEN_KODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    komponen_nama = models.CharField(db_column='KOMPONEN_NAMA', max_length=500, blank=True, null=True)  # Field name made lowercase.
    komponen_spesifikasi = models.TextField(db_column='KOMPONEN_SPESIFIKASI', blank=True, null=True)  # Field name made lowercase.
    komponen_harga = models.FloatField(db_column='KOMPONEN_HARGA', blank=True, null=True)  # Field name made lowercase.
    komponen_satuan = models.CharField(db_column='KOMPONEN_SATUAN', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KOMPONEN'


class RefKomponenRekening(models.Model):
    rekom_id = models.AutoField(db_column='REKOM_ID', primary_key=True)  # Field name made lowercase.
    rekom_tahun = models.IntegerField(db_column='REKOM_TAHUN', blank=True, null=True)  # Field name made lowercase.
    rekening_id = models.IntegerField(db_column='REKENING_ID', blank=True, null=True)  # Field name made lowercase.
    komponen_id = models.IntegerField(db_column='KOMPONEN_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_KOMPONEN_REKENING'


class RefLokasi(models.Model):
    lokasi_id = models.AutoField(db_column='LOKASI_ID', primary_key=True)  # Field name made lowercase.
    lokasi_nama = models.CharField(db_column='LOKASI_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    lokasi_tahun = models.IntegerField(db_column='LOKASI_TAHUN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_LOKASI'


class RefPekerjaan(models.Model):
    pekerjaan_id = models.AutoField(db_column='PEKERJAAN_ID', primary_key=True)  # Field name made lowercase.
    pekerjaan_nama = models.CharField(db_column='PEKERJAAN_NAMA', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_PEKERJAAN'


class RefPersyaratan(models.Model):
    persyaratan_id = models.IntegerField(db_column='PERSYARATAN_ID', primary_key=True)  # Field name made lowercase.
    persyaratan_nama = models.TextField(db_column='PERSYARATAN_NAMA', blank=True, null=True)  # Field name made lowercase.
    isu_id = models.IntegerField(db_column='ISU_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_PERSYARATAN'


class RefProgram(models.Model):
    program_id = models.AutoField(db_column='PROGRAM_ID', primary_key=True)  # Field name made lowercase.
    program_tahun = models.IntegerField(db_column='PROGRAM_TAHUN', blank=True, null=True)  # Field name made lowercase.
    urusan_id = models.IntegerField(db_column='URUSAN_ID', blank=True, null=True)  # Field name made lowercase.
    program_kode = models.CharField(db_column='PROGRAM_KODE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    program_nama = models.CharField(db_column='PROGRAM_NAMA', max_length=300, blank=True, null=True)  # Field name made lowercase.
    program_capaian = models.CharField(db_column='PROGRAM_CAPAIAN', max_length=250, blank=True, null=True)  # Field name made lowercase.
    program_prioritas = models.IntegerField(db_column='PROGRAM_PRIORITAS', blank=True, null=True)  # Field name made lowercase.
    kode_urusan = models.CharField(db_column='KODE_URUSAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_PROGRAM'


class RefProgramCopy1(models.Model):
    program_id = models.AutoField(db_column='PROGRAM_ID', primary_key=True)  # Field name made lowercase.
    program_tahun = models.IntegerField(db_column='PROGRAM_TAHUN', blank=True, null=True)  # Field name made lowercase.
    urusan_id = models.IntegerField(db_column='URUSAN_ID', blank=True, null=True)  # Field name made lowercase.
    program_kode = models.CharField(db_column='PROGRAM_KODE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    program_nama = models.CharField(db_column='PROGRAM_NAMA', max_length=300, blank=True, null=True)  # Field name made lowercase.
    program_capaian = models.CharField(db_column='PROGRAM_CAPAIAN', max_length=250, blank=True, null=True)  # Field name made lowercase.
    program_prioritas = models.IntegerField(db_column='PROGRAM_PRIORITAS', blank=True, null=True)  # Field name made lowercase.
    kode_urusan = models.CharField(db_column='KODE_URUSAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_id = models.IntegerField(db_column='SKPD_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_PROGRAM_copy1'


class RefRekening(models.Model):
    rekening_id = models.AutoField(db_column='REKENING_ID', primary_key=True)  # Field name made lowercase.
    rekening_tahun = models.IntegerField(db_column='REKENING_TAHUN', blank=True, null=True)  # Field name made lowercase.
    rekening_kode = models.CharField(db_column='REKENING_KODE', max_length=20, blank=True, null=True)  # Field name made lowercase.
    rekening_nama = models.CharField(db_column='REKENING_NAMA', max_length=200, blank=True, null=True)  # Field name made lowercase.
    rekening_kunci = models.IntegerField(db_column='REKENING_KUNCI', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_REKENING'


class RefRt(models.Model):
    rt_id = models.AutoField(db_column='RT_ID', primary_key=True)  # Field name made lowercase.
    rt_tahun = models.IntegerField(db_column='RT_TAHUN')  # Field name made lowercase.
    rt_nama = models.CharField(db_column='RT_NAMA', max_length=100)  # Field name made lowercase.
    rt_nik = models.CharField(db_column='RT_NIK', max_length=100, blank=True, null=True)  # Field name made lowercase.
    rt_ketua = models.CharField(db_column='RT_KETUA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    rt_telp = models.CharField(db_column='RT_TELP', max_length=30, blank=True, null=True)  # Field name made lowercase.
    rw_id = models.IntegerField(db_column='RW_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_RT'


class RefRw(models.Model):
    rw_id = models.AutoField(db_column='RW_ID', primary_key=True)  # Field name made lowercase.
    rw_tahun = models.IntegerField(db_column='RW_TAHUN', blank=True, null=True)  # Field name made lowercase.
    rw_nama = models.CharField(db_column='RW_NAMA', max_length=2)  # Field name made lowercase.
    rw_nik = models.CharField(db_column='RW_NIK', max_length=20, blank=True, null=True)  # Field name made lowercase.
    rw_ketua = models.CharField(db_column='RW_KETUA', max_length=200)  # Field name made lowercase.
    rw_telp = models.CharField(db_column='RW_TELP', max_length=12, blank=True, null=True)  # Field name made lowercase.
    kel_id = models.IntegerField(db_column='KEL_ID', blank=True, null=True)  # Field name made lowercase.
    rw_aktif = models.IntegerField(db_column='RW_AKTIF', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_RW'


class RefSasaran(models.Model):
    sasaran_id = models.AutoField(db_column='SASARAN_ID', primary_key=True)  # Field name made lowercase.
    sasaran_nama = models.CharField(db_column='SASARAN_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_SASARAN'


class RefSatuan(models.Model):
    satuan_id = models.AutoField(db_column='SATUAN_ID', primary_key=True)  # Field name made lowercase.
    satuan_nama = models.CharField(db_column='SATUAN_NAMA', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_SATUAN'


class RefSkpd(models.Model):
    skpd_id = models.AutoField(db_column='SKPD_ID', primary_key=True)  # Field name made lowercase.
    skpd_tahun = models.IntegerField(db_column='SKPD_TAHUN', blank=True, null=True)  # Field name made lowercase.
    skpd_kode = models.CharField(db_column='SKPD_KODE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    skpd_nama = models.CharField(db_column='SKPD_NAMA', max_length=200, blank=True, null=True)  # Field name made lowercase.
    skpd_kepala = models.CharField(db_column='SKPD_KEPALA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    skpd_bendahara = models.CharField(db_column='SKPD_BENDAHARA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    skpd_kepala_nip = models.CharField(db_column='SKPD_KEPALA_NIP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    skpd_bendahara_nip = models.CharField(db_column='SKPD_BENDAHARA_NIP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    skpd_bidang = models.IntegerField(db_column='SKPD_BIDANG', blank=True, null=True)  # Field name made lowercase.
    skpd_sub_bidang = models.IntegerField(db_column='SKPD_SUB_BIDANG', blank=True, null=True)  # Field name made lowercase.
    jabatan = models.CharField(db_column='JABATAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nama = models.CharField(db_column='NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nip = models.CharField(db_column='NIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telp = models.CharField(db_column='TELP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pangkat = models.CharField(db_column='PANGKAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status_renja = models.IntegerField(db_column='STATUS_RENJA', blank=True, null=True)  # Field name made lowercase.
    status_posisi = models.IntegerField(db_column='STATUS_POSISI', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_SKPD'


class RefSubBidang(models.Model):
    sub_bidang_id = models.AutoField(db_column='SUB_BIDANG_ID', primary_key=True)  # Field name made lowercase.
    sub_bidang_nama = models.CharField(db_column='SUB_BIDANG_NAMA', max_length=-1)  # Field name made lowercase.
    bidang_id = models.IntegerField(db_column='BIDANG_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_SUB_BIDANG'


class RefSubBidangSkpd(models.Model):
    sub_bidang_skpd_id = models.AutoField(db_column='SUB_BIDANG_SKPD_ID', primary_key=True)  # Field name made lowercase.
    sub_bidang_skpd_nama = models.CharField(db_column='SUB_BIDANG_SKPD_NAMA', max_length=-1)  # Field name made lowercase.
    bidang_skpd_id = models.IntegerField(db_column='BIDANG_SKPD_ID', blank=True, null=True)  # Field name made lowercase.
    nama = models.CharField(db_column='NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    jabatan = models.CharField(db_column='JABATAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nip = models.CharField(db_column='NIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telp = models.CharField(db_column='TELP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pangkat = models.CharField(db_column='PANGKAT', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_SUB_BIDANG_SKPD'


class RefSubKegiatan(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nama_sub_kegiatan = models.TextField(db_column='NAMA_SUB_KEGIATAN')  # Field name made lowercase.
    kegiatan_id = models.IntegerField(db_column='KEGIATAN_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_SUB_KEGIATAN'


class RefSumberDana(models.Model):
    dana_id = models.AutoField(db_column='DANA_ID', primary_key=True)  # Field name made lowercase.
    dana_nama = models.CharField(db_column='DANA_NAMA', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_SUMBER_DANA'


class RefTag(models.Model):
    tag_id = models.AutoField(db_column='TAG_ID', primary_key=True)  # Field name made lowercase.
    tag_nama = models.CharField(db_column='TAG_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    tag_tahun = models.IntegerField(db_column='TAG_TAHUN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_TAG'


class RefTahapan(models.Model):
    tahapan_id = models.AutoField(db_column='TAHAPAN_ID', primary_key=True)  # Field name made lowercase.
    tahapan_nama = models.CharField(db_column='TAHAPAN_NAMA', max_length=-1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_TAHAPAN'


class RefTapd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    jabatan = models.CharField(db_column='JABATAN', max_length=-1)  # Field name made lowercase.
    nama = models.CharField(db_column='NAMA', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nip = models.CharField(db_column='NIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telp = models.CharField(db_column='TELP', max_length=13, blank=True, null=True)  # Field name made lowercase.
    pangkat = models.CharField(db_column='PANGKAT', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_TAPD'


class RefTujuan(models.Model):
    tujuan_id = models.AutoField(db_column='TUJUAN_ID', primary_key=True)  # Field name made lowercase.
    tujuan_tahun = models.IntegerField(db_column='TUJUAN_TAHUN', blank=True, null=True)  # Field name made lowercase.
    tujuan_nama = models.CharField(db_column='TUJUAN_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_TUJUAN'


class RefUrusan(models.Model):
    urusan_id = models.AutoField(db_column='URUSAN_ID', primary_key=True)  # Field name made lowercase.
    kode_urusan = models.CharField(db_column='KODE_URUSAN', max_length=-1)  # Field name made lowercase.
    nama_urusan = models.CharField(db_column='NAMA_URUSAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    skpd_kode = models.CharField(db_column='SKPD_KODE', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REF_URUSAN'
