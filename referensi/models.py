from django.db import models

# Create your models here.
class RefKecamatan(models.Model):
    kec_id = models.AutoField(db_column='KEC_ID', primary_key=True)  # Field name made lowercase.
    kec_tahun = models.IntegerField(db_column='KEC_TAHUN')  # Field name made lowercase.
    kec_nama = models.CharField(db_column='KEC_NAMA', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kec_camat = models.CharField(db_column='KEC_CAMAT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kec_nip = models.CharField(db_column='KEC_NIP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kec_telp = models.CharField(db_column='KEC_TELP', max_length=12, blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return "{}".format(self.kec_id)
    class Meta:
        managed = False
        db_table = u'"REFERENSI\".\"REF_KECAMATAN"'