from django.shortcuts import render
from django.db import connection
from django.http import HttpResponse
from data.models import Users

def grafik1(request):
    # user = Users.object.filter(level=5)
    user=",".join(str(msg.id) for msg in Users.object.filter(level=5))
    sql='SELECT "KEC_NAMA",count("KEL_ID") as jm_kel, sum(COALESCE( rab, 0 )) as rab FROM "DATA".users LEFT JOIN (SELECT "MUSRENBANG"."DAT_USULAN"."USER_CREATED",sum("DAT_PENERIMA"."PENERIMA_RAB") as rab from "MUSRENBANG"."DAT_USULAN" LEFT JOIN "MUSRENBANG"."DAT_PENERIMA" ON "MUSRENBANG"."DAT_USULAN"."USULAN_ID"="MUSRENBANG"."DAT_PENERIMA"."USULAN_ID" where "USER_CREATED" in ('+ user +') GROUP BY "DAT_USULAN"."USER_CREATED" )a on a."USER_CREATED"="DATA".users.id LEFT JOIN "REFERENSI"."REF_KECAMATAN" ON "REFERENSI"."REF_KECAMATAN"."KEC_ID"="DATA".users."KEC_ID" WHERE level=5 GROUP BY "KEC_NAMA" ORDER BY rab asc'
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    context = {
        'title'          : 'Progres Usulan / Kecamatan',
        'active_grafik1'    : 'active',
        'data' :data
    }
    print(data)
    return render(request,'grafik/grafik1.html',context)
def grafik2(request,opd_id=None):
    if opd_id is None:
        a='0'
        wh="where skpd_id="+a
        sql='select * from "MUSRENBANG".new_ajuan_kegiatan_opd '+wh+' order by  skpd_kode asc,program_kode asc, kegiatan_kode asc'
    else:
        user=",".join(str(msg.id) for msg in Users.object.filter(level=5,kec_id=opd_id))
        sql='SELECT name, COALESCE( rab, 0 ) as rab FROM "DATA".users LEFT JOIN ( SELECT "MUSRENBANG"."DAT_USULAN"."USER_CREATED",sum("DAT_PENERIMA"."PENERIMA_RAB") as rab from "MUSRENBANG"."DAT_USULAN" LEFT JOIN "MUSRENBANG"."DAT_PENERIMA" ON "MUSRENBANG"."DAT_USULAN"."USULAN_ID"="MUSRENBANG"."DAT_PENERIMA"."USULAN_ID" where "USER_CREATED" in ('+ user +') GROUP BY "DAT_USULAN"."USER_CREATED" )a on a."USER_CREATED"="DATA".users.id WHERE level=5 AND "KEC_ID"= '+ str(opd_id) +' ORDER BY rab asc'

    cursor = connection.cursor()
    cursor.execute(sql)
    Data = cursor.fetchall()
    cursor = connection.cursor()
    cursor.execute('select "KEC_ID","KEC_NAMA" from "REFERENSI"."REF_KECAMATAN" ORDER BY "KEC_NAMA" ASC ')
    Opd = cursor.fetchall()
    context = {
        'title'          : 'Progres Usulan Desa',
        'active_grafik2'    : 'active',
        'Data'           : Data,
        'Opd'           : Opd,
        'Opd_id'           : opd_id,
    }
    return render(request,'grafik/grafik2.html',context)
def grafik3(request):
    sql='SELECT c."BIDANG_ID" as bid_id,d."BIDANG_NAMA",count(a."USULAN_ID") as jml,sum("public".get_rab(a."USULAN_ID")) as rab FROM "MUSRENBANG"."DAT_USULAN" a JOIN "REFERENSI"."REF_KAMUS" b ON a."KAMUS_ID"=b."KAMUS_ID" JOIN "REFERENSI"."REF_ISU" c ON c."ISU_ID"=b."ISU_ID" JOIN "REFERENSI"."REF_BIDANG" d ON c."BIDANG_ID"=d."BIDANG_ID" WHERE "USULAN_TUJUAN"=6 GROUP BY  bid_id,d."BIDANG_NAMA"'
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    context = {
        'title'          : 'Persentase Usulan Per Bidang',
        'active_grafik3'    : 'active',
        'data' :data
    }
    print(data)
    return render(request,'grafik/grafik3.html',context)

def grafik4(request):
    sql='SELECT c."ISU_ID", c."ISU_NAMA",count(a."USULAN_ID") as jml,sum("public".get_rab(a."USULAN_ID")) as rab FROM "MUSRENBANG"."DAT_USULAN" a JOIN "REFERENSI"."REF_KAMUS" b ON a."KAMUS_ID"=b."KAMUS_ID" JOIN "REFERENSI"."REF_ISU" c ON c."ISU_ID"=b."ISU_ID" WHERE "USULAN_TUJUAN"=6 GROUP BY c."ISU_ID",c."ISU_NAMA" ORDER BY rab desc LIMIT 10'
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    context = {
        'title'          : 'TOP 10 Usulan Per Isu',
        'active_grafik4'    : 'active',
        'data' :data
    }
    print(data)
    return render(request,'grafik/grafik4.html',context)