from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^grafik1/$', views.grafik1, name="grafik1"),
    url(r'^grafik2/$', views.grafik2, name="grafik2"),
    url(r'^grafik2/(?P<opd_id>[0-9]+)/$', views.grafik2, name="grafik2"),
    url(r'^grafik3/$', views.grafik3, name="grafik3"),
    url(r'^grafik4/$', views.grafik4, name="grafik4"),
]